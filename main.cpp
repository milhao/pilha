#include "pilha.hpp"

int main(int argc, char const *argv[]) {

  Pilha p;

  p.empilhar(10);
  p.empilhar(11);
  p.empilhar(12);

  cout << "Pilha Vazia: " << p.vazio() << endl;
  cout << "Topop: " << p.getTopo();
  return 0;
}
