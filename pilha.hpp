#ifndef PILHA_H_INCLUDED
#define PILHA_H_INCLUDED

#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class Pilha {
private:
  int * vet;
  int max_tam;
  int topo;
public:
  Pilha();
  ~Pilha();
  void empilhar(int e);
  void desenpilhar();
  int getTopo();
  int vazio() { return (topo == -1); };
};

#endif
